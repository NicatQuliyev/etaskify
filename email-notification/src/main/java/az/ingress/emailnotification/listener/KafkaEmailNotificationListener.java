package az.ingress.emailnotification.listener;

import az.ingress.emailnotification.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaEmailNotificationListener {

    private final EmailService emailService;

    @KafkaListener(topics = {"email-sender-topic"}, containerFactory = "kafkaListenerContainerFactory")
    public void processEmailMessage(ConsumerRecord<String, String> record) {
        emailService.sendMail(record.key(),"TASK NOTIFICATION",record.value());
    }


}

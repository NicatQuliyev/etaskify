package az.ingress.taskmanagement.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {
    @Bean
    public NewTopic emailNotification() {
        return TopicBuilder.name("email-sender-topic")
                .partitions(3)
                .replicas(1)
                .build();
    }
}

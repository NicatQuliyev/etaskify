package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.AccountRequest;
import az.ingress.taskmanagement.dto.response.AccountResponse;
import az.ingress.taskmanagement.entity.Account;
import az.ingress.taskmanagement.entity.Address;
import az.ingress.taskmanagement.entity.Organization;
import az.ingress.taskmanagement.exception.*;
import az.ingress.taskmanagement.repository.AccountRepository;
import az.ingress.taskmanagement.repository.AddressRepository;
import az.ingress.taskmanagement.repository.OrganizationRepository;
import az.ingress.taskmanagement.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class AccountService {

    private final AccountRepository accountRepository;

    private final AddressRepository addressRepository;

    private final OrganizationRepository organizationRepository;

    private final ModelMapper modelMapper;

    private final TaskRepository taskRepository;

    private final EmailService emailService;

    @Cacheable(cacheNames = "accounts", key = "#root.methodName")
    public List<AccountResponse> findAll() {
        return accountRepository
                .findAll()
                .stream()
                .map(account -> modelMapper.map(account, AccountResponse.class))
                .collect(Collectors.toList());
    }

    @Cacheable(key = "#accountId", cacheNames = "accounts")
    public AccountResponse findById(Long accountId) {
        Account account = accountRepository.findById(accountId).orElseThrow(() ->
                new AccountNotFoundException(ErrorCodes.ACCOUNT_NOT_FOUND, accountId));

        return modelMapper.map(account, AccountResponse.class);
    }

    @CachePut(cacheNames = "accounts")
    public AccountResponse save(AccountRequest accountRequest, Long organizationId, Long addressId) {

        if (accountRepository.existsByEmail(accountRequest.getEmail())) {
            throw new EmailExistException(ErrorCodes.EMAIL_ALREADY_EXIST, accountRequest.getEmail());
        }

        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() ->
                new OrganizationNotFoundException(ErrorCodes.ORGANIZATION_NOT_FOUND, organizationId));

        Address address = addressRepository.findById(addressId).orElseThrow(() ->
                new AddressNotFoundException(ErrorCodes.ADDRESS_NOT_FOUND, addressId));

        Account account = modelMapper.map(accountRequest, Account.class);
        account.setOrganization(organization);
        account.setAddress(address);

        //When Account created by user mail will come to him that he can change own account password.
        emailService.sendMail(account.getEmail(),
                "Your Account has been created by Admin." +
                        "For change your password click the link below",
                "http://localhost:8080/api/v1/accounts/changePassword");

        return modelMapper.map(accountRepository.save(account), AccountResponse.class);
    }

    @CachePut(cacheNames = "accounts")
    public AccountResponse update(AccountRequest accountRequest, Long accountId) {
        accountRepository.findById(accountId).orElseThrow(() ->
                new AccountNotFoundException(ErrorCodes.ACCOUNT_NOT_FOUND, accountId));

        Account account = modelMapper.map(accountRequest, Account.class);
        account.setId(accountId);

        return modelMapper.map(accountRepository.save(account), AccountResponse.class);
    }

    public void delete(Long accountId) {
        var tasks = taskRepository.findAllByAccountId(accountId);
        Account account = accountRepository.findById(accountId).orElseThrow(() ->
                new AccountNotFoundException(ErrorCodes.ACCOUNT_NOT_FOUND, accountId));

        tasks.forEach(task -> {
            task.getAccounts().remove(account);
        });

        taskRepository.saveAll(tasks);


        accountRepository.delete(account);
    }

}

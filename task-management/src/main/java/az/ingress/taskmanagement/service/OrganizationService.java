package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.OrganizationRequest;
import az.ingress.taskmanagement.dto.response.OrganizationResponse;
import az.ingress.taskmanagement.entity.Organization;
import az.ingress.taskmanagement.exception.ErrorCodes;
import az.ingress.taskmanagement.exception.OrganizationNotFoundException;
import az.ingress.taskmanagement.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrganizationService {

    private final OrganizationRepository organizationRepository;

    private final ModelMapper modelMapper;

    @Cacheable(cacheNames = "organizations", key = "#root.methodName")
    public List<OrganizationResponse> findAll() {
        return organizationRepository
                .findAll()
                .stream()
                .map(organization -> modelMapper.map(organization, OrganizationResponse.class))
                .collect(Collectors.toList());
    }

    @Cacheable(key = "#organizationId", cacheNames = "organizations")
    public OrganizationResponse findById(Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() ->
                new OrganizationNotFoundException(ErrorCodes.ORGANIZATION_NOT_FOUND, organizationId));

        return modelMapper.map(organization, OrganizationResponse.class);
    }
    @CachePut(cacheNames = "organizations")
    public OrganizationResponse save(OrganizationRequest request) {
        Organization organization = modelMapper.map(request, Organization.class);

        return modelMapper.map(organizationRepository.save(organization), OrganizationResponse.class);
    }
    @CachePut(cacheNames = "organizations")
    public OrganizationResponse update(Long organizationId, OrganizationRequest request) {
        organizationRepository.findById(organizationId).orElseThrow(() ->
                new OrganizationNotFoundException(ErrorCodes.ORGANIZATION_NOT_FOUND, organizationId));

        Organization organization = modelMapper.map(request, Organization.class);
        organization.setId(organizationId);

        return modelMapper.map(organizationRepository.save(organization), OrganizationResponse.class);
    }

    public void delete(Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() ->
                new OrganizationNotFoundException(ErrorCodes.ORGANIZATION_NOT_FOUND, organizationId));

        organizationRepository.delete(organization);
    }


}

package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.AddressRequest;
import az.ingress.taskmanagement.dto.response.AddressResponse;
import az.ingress.taskmanagement.entity.Address;
import az.ingress.taskmanagement.exception.AddressNotFoundException;
import az.ingress.taskmanagement.exception.ErrorCodes;
import az.ingress.taskmanagement.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;

    private final ModelMapper modelMapper;

    @Cacheable(cacheNames = "addresses", key = "#root.methodName")
    public List<AddressResponse> findAll() {
        return addressRepository
                .findAll()
                .stream()
                .map(address -> modelMapper.map(address, AddressResponse.class))
                .collect(Collectors.toList());
    }

    @Cacheable(key = "#addressId", cacheNames = "addresses")
    public AddressResponse findById(Long addressId) {
        Address address = addressRepository.findById(addressId).orElseThrow(() ->
                new AddressNotFoundException(ErrorCodes.ADDRESS_NOT_FOUND, addressId));

        return modelMapper.map(address, AddressResponse.class);
    }
    @CachePut(cacheNames = "addresses")
    public AddressResponse save(AddressRequest request) {
        Address address = modelMapper.map(request, Address.class);

        return modelMapper.map(addressRepository.save(address), AddressResponse.class);
    }
    @CachePut(cacheNames = "addresses")
    public AddressResponse update(Long addressId, AddressRequest request) {
        addressRepository.findById(addressId).orElseThrow(() ->
                new AddressNotFoundException(ErrorCodes.ADDRESS_NOT_FOUND, addressId));

        Address address = modelMapper.map(request, Address.class);
        address.setId(addressId);

        return modelMapper.map(addressRepository.save(address), AddressResponse.class);
    }

    public void delete(Long addressId) {
        Address address = addressRepository.findById(addressId).orElseThrow(() ->
                new AddressNotFoundException(ErrorCodes.ADDRESS_NOT_FOUND, addressId));

        addressRepository.delete(address);
    }


}

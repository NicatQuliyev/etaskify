package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.AccountListDto;
import az.ingress.taskmanagement.dto.request.TaskRequest;
import az.ingress.taskmanagement.dto.response.TaskResponse;
import az.ingress.taskmanagement.entity.Account;
import az.ingress.taskmanagement.entity.Task;
import az.ingress.taskmanagement.exception.AccountOrOrganizationNotFoundException;
import az.ingress.taskmanagement.exception.ErrorCodes;
import az.ingress.taskmanagement.exception.TaskNotFoundException;
import az.ingress.taskmanagement.repository.AccountRepository;
import az.ingress.taskmanagement.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    private final AccountRepository accountRepository;

    private final ModelMapper modelMapper;

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Cacheable(cacheNames = "tasks", key = "#root.methodName")
    public List<TaskResponse> findAll() {
        return taskRepository
                .findAll()
                .stream()
                .map(task -> modelMapper.map(task, TaskResponse.class))
                .collect(Collectors.toList());
    }

    @Cacheable(key = "#id", cacheNames = "tasks")
    public TaskResponse findById(Long id) {
        Task task = taskRepository.findById(id).orElseThrow(() ->
                new TaskNotFoundException(ErrorCodes.TASK_NOT_FOUND, id));

        return modelMapper.map(task, TaskResponse.class);
    }
    @CachePut(cacheNames = "tasks")
    public TaskResponse save(TaskRequest request) {
        Task task = modelMapper.map(request, Task.class);

        return modelMapper.map(taskRepository.save(task), TaskResponse.class);
    }
    @CachePut(cacheNames = "tasks")
    public TaskResponse update(Long id, TaskRequest request) {
        taskRepository.findById(id).orElseThrow(() ->
                new TaskNotFoundException(ErrorCodes.TASK_NOT_FOUND, id));


        Task task = modelMapper.map(request, Task.class);
        task.setId(id);

        return modelMapper.map(taskRepository.save(task), TaskResponse.class);
    }

    public void delete(Long id) {
        Task task = taskRepository.findById(id).orElseThrow(() ->
                new TaskNotFoundException(ErrorCodes.TASK_NOT_FOUND, id));

        taskRepository.delete(task);
    }

    public Task assignAccountsToTask(Long organizationId, Long taskId, AccountListDto accountListDto) {
        Task task = taskRepository.findById(taskId).orElseThrow(() ->
                new TaskNotFoundException(ErrorCodes.TASK_NOT_FOUND, taskId));

        for (Long accountId : accountListDto.getAccountIds()) {
            Account account = accountRepository.findByOrganizationIdAndId(organizationId, accountId)
                    .orElseThrow(() -> new AccountOrOrganizationNotFoundException(ErrorCodes.ACCOUNT_OR_ORGANIZATION_NOT_FOUND));
            task.getAccounts().add(account);

            List<Header> headers = new ArrayList<>();
            headers.add(new RecordHeader("Accept-Language", "az".getBytes()));
            kafkaTemplate.send(new ProducerRecord<String, String>("email-sender-topic", 1, account.getEmail(), "YOU HAVE NEW TASK"));

        }


        return taskRepository.save(task);
    }
}

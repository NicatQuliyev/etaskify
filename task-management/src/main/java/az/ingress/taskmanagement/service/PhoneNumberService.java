package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.PhoneNumberRequest;
import az.ingress.taskmanagement.dto.response.PhoneNumberResponse;
import az.ingress.taskmanagement.entity.Account;
import az.ingress.taskmanagement.entity.PhoneNumber;
import az.ingress.taskmanagement.exception.AccountNotFoundException;
import az.ingress.taskmanagement.exception.ErrorCodes;
import az.ingress.taskmanagement.exception.PhoneNumberNotFoundException;
import az.ingress.taskmanagement.repository.AccountRepository;
import az.ingress.taskmanagement.repository.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PhoneNumberService {

    private final AccountRepository accountRepository;

    private final PhoneNumberRepository numberRepository;

    private final ModelMapper modelMapper;

    @Cacheable(cacheNames = "phone-number", key = "#root.methodName")
    public List<PhoneNumberResponse> findAll() {
        return numberRepository
                .findAll()
                .stream()
                .map(number -> modelMapper.map(number, PhoneNumberResponse.class))
                .collect(Collectors.toList());
    }

    @Cacheable(key = "#numberId", cacheNames = "phone-number")
    public PhoneNumberResponse findById(Long numberId) {
        PhoneNumber number = numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND, numberId));

        return modelMapper.map(number, PhoneNumberResponse.class);
    }
    @CachePut(cacheNames = "phone-number")
    public PhoneNumberResponse save(PhoneNumberRequest request, Long accountId) {
        Account account = accountRepository.findById(accountId).orElseThrow(() ->
                new AccountNotFoundException(ErrorCodes.ACCOUNT_NOT_FOUND, accountId));

        PhoneNumber number = modelMapper.map(request, PhoneNumber.class);
        number.setAccount(account);

        return modelMapper.map(numberRepository.save(number), PhoneNumberResponse.class);
    }
    @CachePut(cacheNames = "phone-number")
    public PhoneNumberResponse update(PhoneNumberRequest request, Long numberId) {
        numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND, numberId));

        PhoneNumber number = modelMapper.map(request, PhoneNumber.class);
        number.setId(numberId);

        return modelMapper.map(numberRepository.save(number), PhoneNumberResponse.class);
    }

    public void delete(Long numberId) {
        PhoneNumber number = numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND, numberId));

        numberRepository.delete(number);
    }
}

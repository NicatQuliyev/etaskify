package az.ingress.taskmanagement.controller;

import az.ingress.taskmanagement.dto.request.OrganizationRequest;
import az.ingress.taskmanagement.dto.response.OrganizationResponse;
import az.ingress.taskmanagement.service.OrganizationService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/organizations")
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "bearerAuth")
public class OrganizationController {

    private final OrganizationService organizationService;

    @GetMapping
    public ResponseEntity<List<OrganizationResponse>> findAll() {
        return new ResponseEntity<>(organizationService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{organizationId}")
    public ResponseEntity<OrganizationResponse> findById(@PathVariable Long organizationId) {
        return new ResponseEntity<>(organizationService.findById(organizationId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OrganizationResponse> save(@RequestBody OrganizationRequest request) {
        return new ResponseEntity<>(organizationService.save(request), HttpStatus.CREATED);
    }

    @PutMapping("/{organizationId}")
    public ResponseEntity<OrganizationResponse> update(@RequestBody OrganizationRequest request,
                                                       @PathVariable Long organizationId) {
        return new ResponseEntity<>(organizationService.update(organizationId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{organizationId}")
    public void delete(@PathVariable Long organizationId) {
        organizationService.delete(organizationId);
    }
}

package az.ingress.taskmanagement.controller;

import az.ingress.taskmanagement.dto.request.PhoneNumberRequest;
import az.ingress.taskmanagement.dto.response.PhoneNumberResponse;
import az.ingress.taskmanagement.service.PhoneNumberService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/phone-number")
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "bearerAuth")
public class PhoneNumberController {

    private final PhoneNumberService phoneNumberService;

    @GetMapping
    public ResponseEntity<List<PhoneNumberResponse>> findAll() {
        return new ResponseEntity<>(phoneNumberService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{numberId}")
    public ResponseEntity<PhoneNumberResponse> findById(@PathVariable Long numberId) {
        return new ResponseEntity<>(phoneNumberService.findById(numberId), HttpStatus.OK);
    }

    @PostMapping("/account/{accountId}")
    public ResponseEntity<PhoneNumberResponse> save(@RequestBody PhoneNumberRequest request,
                                                    @PathVariable Long accountId) {
        return new ResponseEntity<>(phoneNumberService.save(request, accountId), HttpStatus.CREATED);
    }

    @PutMapping("/{numberId}")
    public ResponseEntity<PhoneNumberResponse> update(@RequestBody PhoneNumberRequest request,
                                                      @PathVariable Long numberId) {
        return new ResponseEntity<>(phoneNumberService.update(request, numberId), HttpStatus.OK);
    }

    @DeleteMapping("/{numberId}")
    public void delete(@PathVariable Long numberId) {
        phoneNumberService.delete(numberId);
    }
}

package az.ingress.taskmanagement.controller;

import az.ingress.taskmanagement.dto.request.AccountListDto;
import az.ingress.taskmanagement.dto.request.TaskRequest;
import az.ingress.taskmanagement.dto.response.TaskResponse;
import az.ingress.taskmanagement.entity.Task;
import az.ingress.taskmanagement.service.TaskService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/tasks")
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
public class TaskController {

    private final TaskService taskService;

    @GetMapping
    public ResponseEntity<List<TaskResponse>> findAll() {
        return new ResponseEntity<>(taskService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{taskId}")
    public ResponseEntity<TaskResponse> findById(@PathVariable Long taskId) {
        return new ResponseEntity<>(taskService.findById(taskId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TaskResponse> save(@RequestBody TaskRequest request) {
        return new ResponseEntity<>(taskService.save(request), HttpStatus.CREATED);
    }

    @PostMapping("/organizations/{organizationId}/tasks/{taskId}/accounts")
    public ResponseEntity<Task> assignUsersToTask(@PathVariable Long organizationId,
                                                  @PathVariable Long taskId,
                                                  @RequestBody AccountListDto accountIds) {

        return new ResponseEntity<>(taskService.assignAccountsToTask(organizationId, taskId, accountIds), HttpStatus.CREATED);
    }

    @PutMapping("/{taskId}")
    public ResponseEntity<TaskResponse> update(@PathVariable Long taskId,
                                               @RequestBody TaskRequest request) {
        return new ResponseEntity<>(taskService.update(taskId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{taskId}")
    public void delete(@PathVariable Long taskId) {
        taskService.delete(taskId);
    }

}

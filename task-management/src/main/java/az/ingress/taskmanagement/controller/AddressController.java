package az.ingress.taskmanagement.controller;

import az.ingress.taskmanagement.dto.request.AddressRequest;
import az.ingress.taskmanagement.dto.response.AddressResponse;
import az.ingress.taskmanagement.service.AddressService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/addresses")
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "bearerAuth")
public class AddressController {

    private final AddressService addressService;

    @GetMapping
    public ResponseEntity<List<AddressResponse>> findAll() {
        return new ResponseEntity<>(addressService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{addressId}")
    public ResponseEntity<AddressResponse> findById(@PathVariable Long addressId) {
        return new ResponseEntity<>(addressService.findById(addressId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<AddressResponse> save(@RequestBody AddressRequest request) {
        return new ResponseEntity<>(addressService.save(request), HttpStatus.CREATED);
    }

    @PutMapping("/{addressId}")
    public ResponseEntity<AddressResponse> update(@RequestBody AddressRequest request,
                                                  @PathVariable Long addressId) {
        return new ResponseEntity<>(addressService.update(addressId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{addressId}")
    public void delete(@PathVariable Long addressId) {
        addressService.delete(addressId);
    }
}

package az.ingress.taskmanagement.controller;

import az.ingress.taskmanagement.dto.request.AccountRequest;
import az.ingress.taskmanagement.dto.response.AccountResponse;
import az.ingress.taskmanagement.service.AccountService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/accounts")
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "bearerAuth")
public class AccountController {

    private final AccountService accountService;

    @GetMapping
    public ResponseEntity<List<AccountResponse>> findAll() {
        return new ResponseEntity<>(accountService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<AccountResponse> findById(@PathVariable Long accountId) {
        return new ResponseEntity<>(accountService.findById(accountId), HttpStatus.OK);
    }

    @PostMapping("/organization/{organizationId}/address/{addressId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AccountResponse> save(@RequestBody AccountRequest request,
                                                @PathVariable Long organizationId,
                                                @PathVariable Long addressId) {
        return new ResponseEntity<>(accountService.save(request, organizationId, addressId), HttpStatus.CREATED);
    }

    @PutMapping("/{accountId}")
    public ResponseEntity<AccountResponse> update(@RequestBody AccountRequest request,
                                                  @PathVariable Long accountId) {
        return new ResponseEntity<>(accountService.update(request, accountId), HttpStatus.OK);
    }

    @DeleteMapping("/{accountId}")
    public void delete(@PathVariable Long accountId) {
        accountService.delete(accountId);
    }
}

package az.ingress.taskmanagement.repository;

import az.ingress.taskmanagement.entity.Authority;
import az.ingress.taskmanagement.entity.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Optional<Authority> findByAuthority(UserAuthority userAuthority);
}

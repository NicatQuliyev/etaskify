package az.ingress.taskmanagement.repository;

import az.ingress.taskmanagement.entity.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {


}

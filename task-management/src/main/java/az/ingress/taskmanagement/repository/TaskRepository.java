package az.ingress.taskmanagement.repository;

import az.ingress.taskmanagement.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query(value = "select t from Task t join fetch t.accounts a where a.id = :id")
    Collection<Task> findAllByAccountId(Long id);

}

package az.ingress.taskmanagement.repository;

import az.ingress.taskmanagement.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}

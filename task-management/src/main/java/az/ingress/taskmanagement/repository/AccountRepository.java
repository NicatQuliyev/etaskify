package az.ingress.taskmanagement.repository;

import az.ingress.taskmanagement.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Boolean existsByEmail(String email);

    Optional<Account> findByOrganizationIdAndId(Long organizationId, Long userId);


}

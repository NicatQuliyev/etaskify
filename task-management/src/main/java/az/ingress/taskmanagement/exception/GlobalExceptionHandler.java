package az.ingress.taskmanagement.exception;

import az.ingress.taskmanagement.service.TranslationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private final TranslationService translationService;


    @ExceptionHandler(EmailExistException.class)
    public ResponseEntity<ErrorResponseDto> handleEmailExistException(EmailExistException ex,
                                                                      WebRequest req) {

        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.arguments))
                .build());
    }

    @ExceptionHandler(UserNameExistException.class)
    public ResponseEntity<ErrorResponseDto> handleUserNameExistException(UserNameExistException ex,
                                                                         WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.arguments))
                .build());
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleAccountNotFoundException(AccountNotFoundException ex,
                                                                           WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.arguments))
                .build());
    }

    @ExceptionHandler(OrganizationNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleOrganizationNotFoundException(OrganizationNotFoundException ex,
                                                                                WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.arguments))
                .build());
    }

    @ExceptionHandler(AddressNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleAddressNotFoundException(AddressNotFoundException ex,
                                                                           WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.arguments))
                .build());
    }

    @ExceptionHandler(PhoneNumberNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleAddressNotFoundException(PhoneNumberNotFoundException ex,
                                                                           WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.arguments))
                .build());
    }

    @ExceptionHandler(AccountOrOrganizationNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleAccountOrOrganizationNotFoundException(AccountOrOrganizationNotFoundException ex,
                                                                                         WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang))
                .build());
    }

    @ExceptionHandler(TaskNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleTaskNotFoundException(TaskNotFoundException ex,
                                                                        WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.arguments))
                .build());
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleUserNotFoundException(UserNotFoundException ex,
                                                                        WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(404).body(ErrorResponseDto.builder()
                .status(404)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.arguments))
                .build());
    }

    @ExceptionHandler(InvalidPasswordException.class)
    public ResponseEntity<ErrorResponseDto> handleInvalidPasswordException(InvalidPasswordException ex,
                                                                           WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(404).body(ErrorResponseDto.builder()
                .status(404)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang))
                .build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex,
                                                                                  WebRequest req) {
        ex.printStackTrace();
        ErrorResponseDto response = ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details("Validation Error")
                .build();

        ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .forEach(error -> {
                    Map<String, String> data = response.getData();
                    data.put(error.getField(), "password must be 6 or more alphanumeric characters.");
                });
        return ResponseEntity.status(400).body(response);
    }
}

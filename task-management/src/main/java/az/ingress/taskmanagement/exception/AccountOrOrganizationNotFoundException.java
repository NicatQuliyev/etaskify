package az.ingress.taskmanagement.exception;

import lombok.Getter;

import java.util.Arrays;

@Getter
public class AccountOrOrganizationNotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;


    public AccountOrOrganizationNotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}

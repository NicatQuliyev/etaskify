package az.ingress.taskmanagement.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PhoneNumberResponse implements Serializable {

    public static final long serialVersionUID = 1234123456125L;

    Long id;

    String phoneNumber;

}

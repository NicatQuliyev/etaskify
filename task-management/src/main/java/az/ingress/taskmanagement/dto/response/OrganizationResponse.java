package az.ingress.taskmanagement.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrganizationResponse implements Serializable {

     public static final long serialVersionUID = 1234123456126L;

     Long id;

     String organizationName;

     @Builder.Default
     List<AccountResponse> accounts = new ArrayList<>();
}

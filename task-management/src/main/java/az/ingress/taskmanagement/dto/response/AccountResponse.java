package az.ingress.taskmanagement.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class AccountResponse implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    Long id;

    String name;

    String surname;

    String email;

    String password;

    @Builder.Default
    List<PhoneNumberResponse> phoneNumbers = new ArrayList<>();

    AddressResponse address;
}

package az.ingress.taskmanagement.dto.request;

import az.ingress.taskmanagement.entity.TaskStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskRequest {

     String title;

     String description;

     LocalDate deadline;

     TaskStatus taskStatus;
}

package az.ingress.taskmanagement.dto.response;

import az.ingress.taskmanagement.entity.TaskStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskResponse implements Serializable {

    public static final long serialVersionUID = 1234123456127L;

    Long id;

    String title;

    LocalDate deadline;

    TaskStatus taskStatus;

    String description;

    @Builder.Default
    List<AccountResponse> accounts = new ArrayList<>();
}
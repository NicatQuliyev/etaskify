package az.ingress.taskmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "accounts")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String surname;

    private String email;

    private String password;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    Organization organization;

    @OneToOne
    @JoinColumn(name = "address_id")
    @JsonIgnore
    @ToString.Exclude
    Address address;

    @OneToMany(mappedBy = "account", cascade = CascadeType.REMOVE)
    List<PhoneNumber> phoneNumbers = new ArrayList<>();
}

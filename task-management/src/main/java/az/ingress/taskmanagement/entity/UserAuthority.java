package az.ingress.taskmanagement.entity;

public enum UserAuthority {
    USER,
    ADMIN
}

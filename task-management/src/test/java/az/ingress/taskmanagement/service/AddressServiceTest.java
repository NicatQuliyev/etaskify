package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.AddressRequest;
import az.ingress.taskmanagement.dto.response.AddressResponse;
import az.ingress.taskmanagement.entity.Address;
import az.ingress.taskmanagement.exception.AddressNotFoundException;
import az.ingress.taskmanagement.exception.ErrorCodes;
import az.ingress.taskmanagement.repository.AddressRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AddressServiceTest {

    @InjectMocks
    private AddressService addressService;

    @Mock
    private AddressRepository addressRepository;
    @Mock
    private ModelMapper modelMapper;

    @Test
    void givenAllAddressWhenGetSuccess() {
        //Arrange

        List<Address> addressList = new ArrayList<>();

        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        addressList.add(mockAddress);

        when(addressRepository.findAll()).thenReturn(addressList);


        List<AddressResponse> mockAddressResponseList = new ArrayList<>();

        AddressResponse mockAddressResponse = AddressResponse.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        mockAddressResponseList.add(mockAddressResponse);


        when(modelMapper.map(addressList.get(0), AddressResponse.class)).thenReturn(mockAddressResponseList.get(0));


        //Act
        List<AddressResponse> addressResponseList = addressService.findAll();

        //Assert
        assertEquals(mockAddressResponseList, addressResponseList);
    }

    @Test
    void givenValidIdWhenGetAddressThenSuccess() {
        //Arrange
        long id = 1L;


        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        when(addressRepository.findById(id)).thenReturn(Optional.of(mockAddress));

        AddressResponse mockAddressResponse = AddressResponse.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        when(modelMapper.map(mockAddress, AddressResponse.class)).thenReturn(mockAddressResponse);


        //Act
        AddressResponse addressResponse = addressService.findById(1L);

        //Assert
        assertEquals(addressResponse.getId(), mockAddressResponse.getId());
    }

    @Test
    public void givenInValidIdWhenGetAddressThenException() {
        // Arrange
        Long addressId = 1L;
        when(addressRepository.findById(addressId)).thenReturn(Optional.empty());

        // Act and Assert
        AddressNotFoundException exception = assertThrows(AddressNotFoundException.class, () -> {
            addressService.findById(addressId);
        });

        assertEquals(ErrorCodes.ADDRESS_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void saveAddressWhenSuccess() {
        //Arrange
        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        when(addressRepository.save(any())).thenReturn(mockAddress);

        AddressRequest addressRequestDto = AddressRequest.builder()
                .address("20 yanvar")
                .build();

        when(modelMapper.map(addressRequestDto, Address.class)).thenReturn(mockAddress);

        AddressResponse mockAddressResponse = AddressResponse.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        when(modelMapper.map(mockAddress, AddressResponse.class)).thenReturn(mockAddressResponse);


        //Act
        AddressResponse addressResponse = addressService.save(addressRequestDto);


        //Assert
        assertEquals(mockAddressResponse, addressResponse);
    }

    @Test
    void updateAddressWhenSuccess() {
        //Arrange
        Address mockAddress = Address.builder()
                .id(1L)
                .address("ecemi")
                .build();

        when(addressRepository.findById(1L)).thenReturn(Optional.of(mockAddress));

        AddressRequest addressRequestDto = AddressRequest.builder()
                .address("20 yanvar")
                .build();

        when(modelMapper.map(any(AddressRequest.class), eq(Address.class)))
                .thenReturn(mockAddress);

        Address updatedAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();
        when(addressRepository.save(any(Address.class))).thenReturn(updatedAddress);

        AddressResponse mockAddressResponse = AddressResponse.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        when(modelMapper.map(any(Address.class), eq(AddressResponse.class)))
                .thenReturn(mockAddressResponse);

        //Act
        addressService.update(1L, addressRequestDto);

        //Assert
        assertThat(mockAddressResponse.getId()).isEqualTo(1L);
        assertThat(mockAddressResponse.getAddress()).isEqualTo("20 yanvar");
    }

    @Test
    void givenInvalidWhenUpdateAddressThenNotFound() {
        //Arrange
        Long id = 1L;
        when(addressRepository.findById(id)).thenReturn(Optional.empty());

        AddressRequest addressRequest = AddressRequest.builder()
                .address("ecemi")
                .build();

        //Act&Assert
        AddressNotFoundException exception = assertThrows(AddressNotFoundException.class, () -> {
            addressService.update(id, addressRequest);
        });

        assertEquals(ErrorCodes.ADDRESS_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void deleteAddressWhenSuccess() {
        // Arrange

        Address mockAddress = Address.builder()
                .id(1L)
                .address("ecemi")
                .build();

        when(addressRepository.findById(mockAddress.getId())).thenReturn(Optional.of(mockAddress));


        // Act
        addressService.delete(mockAddress.getId());

        // Assert
        verify(addressRepository, times(1)).delete(mockAddress);
    }

    @Test
    void givenInvalidWhenDeleteAddressIsFail() {
        // Arrange
        Long id = 1L;
        when(addressRepository.findById(id)).thenReturn(Optional.empty());

        // Act&Assert
        AddressNotFoundException exception = assertThrows(AddressNotFoundException.class, () -> {
            addressService.delete(id);
        });

        assertEquals(ErrorCodes.ADDRESS_NOT_FOUND, exception.getErrorCode());
    }
}
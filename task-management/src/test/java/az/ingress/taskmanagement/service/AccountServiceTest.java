package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.AccountRequest;
import az.ingress.taskmanagement.dto.response.AccountResponse;
import az.ingress.taskmanagement.dto.response.AddressResponse;
import az.ingress.taskmanagement.entity.Account;
import az.ingress.taskmanagement.entity.Address;
import az.ingress.taskmanagement.entity.Organization;
import az.ingress.taskmanagement.entity.Task;
import az.ingress.taskmanagement.exception.*;
import az.ingress.taskmanagement.repository.AccountRepository;
import az.ingress.taskmanagement.repository.AddressRepository;
import az.ingress.taskmanagement.repository.OrganizationRepository;
import az.ingress.taskmanagement.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private EmailService emailService;

    @Mock
    private ModelMapper modelMapper;

    @Test
    void givenAllAccountWhenGetSuccess() {
        //Arrange

        List<Account> accountList = new ArrayList<>();

        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        AddressResponse mockAddressResponse = AddressResponse.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        List phoneNumbers = List.of("+994502858581", "+994997288873");

        Account mockAccount = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();

        accountList.add(mockAccount);

        when(accountRepository.findAll()).thenReturn(accountList);


        List<AccountResponse> mockAccountResponseList = new ArrayList<>();

        AccountResponse mockAccountResponse = AccountResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddressResponse)
                .phoneNumbers(phoneNumbers)
                .build();

        mockAccountResponseList.add(mockAccountResponse);


        when(modelMapper.map(accountList.get(0), AccountResponse.class)).thenReturn(mockAccountResponseList.get(0));


        //Act
        List<AccountResponse> accountResponseList = accountService.findAll();

        //Assert
        assertEquals(mockAccountResponseList, accountResponseList);
    }

    @Test
    void givenValidIdWhenGetAccountThenSuccess() {
        //Arrange
        long id = 1L;

        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        AddressResponse mockAddressResponse = AddressResponse.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        List phoneNumbers = List.of("+994502858581", "+994997288873");

        Account mockAccount = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();

        when(accountRepository.findById(id)).thenReturn(Optional.of(mockAccount));

        AccountResponse mockAccountResponse = AccountResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddressResponse)
                .phoneNumbers(phoneNumbers)
                .build();

        when(modelMapper.map(mockAccount, AccountResponse.class)).thenReturn(mockAccountResponse);


        //Act
        AccountResponse accountResponse = accountService.findById(1L);

        //Assert
        assertEquals(accountResponse.getId(), mockAccountResponse.getId());
    }

    @Test
    public void givenInValidIdWhenGetAccountThenException() {
        // Arrange
        Long accountId = 1L;
        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        // Act and Assert
        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            accountService.findById(accountId);
        });

        assertEquals(ErrorCodes.ACCOUNT_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    public void testSaveEmailExists() {
        // Arrange
        AccountRequest accountRequest = new AccountRequest();
        accountRequest.setEmail("quliyevv.nicat2003@gmail.com");

        when(accountRepository.existsByEmail(accountRequest.getEmail())).thenReturn(true);

        // Act and Assert
        EmailExistException exception = assertThrows(EmailExistException.class, () -> {
            accountService.save(accountRequest, 1L, 1L);
        });

        assertEquals(ErrorCodes.EMAIL_ALREADY_EXIST, exception.getErrorCode());
    }

    @Test
    public void testSaveOrganizationNotFound() {
        // Arrange
        AccountRequest accountRequest = new AccountRequest();
        Long organizationId = 1L;
        Long addressId = 1L;

        when(accountRepository.existsByEmail(accountRequest.getEmail())).thenReturn(false);
        when(organizationRepository.findById(organizationId)).thenReturn(Optional.empty());

        // Act and Assert
        OrganizationNotFoundException exception = assertThrows(OrganizationNotFoundException.class, () -> {
            accountService.save(accountRequest, organizationId, addressId);
        });

        assertEquals(ErrorCodes.ORGANIZATION_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    public void testSaveAddressNotFound() {
        // Arrange
        AccountRequest accountRequest = new AccountRequest();
        Long organizationId = 1L;
        Long addressId = 1L;
        Organization organization = new Organization();

        when(accountRepository.existsByEmail(accountRequest.getEmail())).thenReturn(false);
        when(organizationRepository.findById(organizationId)).thenReturn(Optional.of(organization));
        when(addressRepository.findById(addressId)).thenReturn(Optional.empty());

        // Act and Assert
        AddressNotFoundException exception = assertThrows(AddressNotFoundException.class, () -> {
            accountService.save(accountRequest, organizationId, addressId);
        });

        assertEquals(ErrorCodes.ADDRESS_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    public void testSaveSuccessful() {
        // Arrange
        AccountRequest accountRequest = new AccountRequest();
        Long organizationId = 1L;
        Long addressId = 1L;
        Organization organization = new Organization();
        Address address = new Address();
        Account account = new Account();
        AccountResponse expectedResponse = new AccountResponse();

        when(accountRepository.existsByEmail(accountRequest.getEmail())).thenReturn(false);
        when(organizationRepository.findById(organizationId)).thenReturn(Optional.of(organization));
        when(addressRepository.findById(addressId)).thenReturn(Optional.of(address));
        when(modelMapper.map(accountRequest, Account.class)).thenReturn(account);
        when(accountRepository.save(account)).thenReturn(account);
        when(modelMapper.map(account, AccountResponse.class)).thenReturn(expectedResponse);

        // Act
        AccountResponse actualResponse = accountService.save(accountRequest, organizationId, addressId);

        // Assert
        assertEquals(expectedResponse, actualResponse);
        assertEquals(organization, account.getOrganization());
        assertEquals(address, account.getAddress());
        verify(emailService).sendMail(account.getEmail(), "Your Account has been created by Admin." +
                "For change your password click the link below", "http://localhost:8080/api/v1/accounts/changePassword");
    }

    @Test
    void updateAccountWhenSuccess() {
        //Arrange
        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        AddressResponse mockAddressResponse = AddressResponse.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        List phoneNumbers = List.of("+994502858581", "+994997288873");

        Account mockAccount = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();

        when(accountRepository.findById(1L)).thenReturn(Optional.of(mockAccount));

        AccountRequest accountRequest = AccountRequest.builder()
                .name("Ali")
                .surname("Quliyev")
                .email("quliyevv.ali@gmail.com")
                .password("qwerty")
                .build();

        when(modelMapper.map(any(AccountRequest.class), eq(Account.class)))
                .thenReturn(mockAccount);

        Account updatedAccount = Account.builder()
                .name("Ali")
                .surname("Quliyev")
                .email("quliyevv.ali@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();
        when(accountRepository.save(any(Account.class))).thenReturn(updatedAccount);

        AccountResponse mockAccountResponse = AccountResponse.builder()
                .id(1L)
                .name("Ali")
                .surname("Quliyev")
                .email("quliyevv.ali@gmail.com")
                .password("qwerty")
                .address(mockAddressResponse)
                .phoneNumbers(phoneNumbers)
                .build();

        when(modelMapper.map(any(Account.class), eq(AccountResponse.class)))
                .thenReturn(mockAccountResponse);

        //Act
        accountService.update(accountRequest, 1L);

        //Assert
        assertThat(mockAccountResponse.getId()).isEqualTo(1L);
        assertThat(mockAccountResponse.getName()).isEqualTo("Ali");
        assertThat(mockAccountResponse.getSurname()).isEqualTo("Quliyev");
        assertThat(mockAccountResponse.getEmail()).isEqualTo("quliyevv.ali@gmail.com");
        assertThat(mockAccountResponse.getPassword()).isEqualTo("qwerty");
    }

    @Test
    void givenInvalidWhenUpdateAccountThenNotFound() {
        //Arrange
        Long id = 1L;
        when(accountRepository.findById(id)).thenReturn(Optional.empty());

        AccountRequest accountRequest = AccountRequest.builder()
                .name("Ali")
                .surname("Quliyev")
                .email("quliyevv.ali@gmail.com")
                .password("qwerty")
                .build();

        //Act&Assert
        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            accountService.update(accountRequest, id);
        });

        assertEquals(ErrorCodes.ACCOUNT_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void deleteAccountWhenSuccess() {
        // Arrange
        Long accountId = 1L;
        Account account = new Account();
        List<Task> tasks = new ArrayList<>();
        Task task1 = new Task();
        Task task2 = new Task();

        tasks.add(task1);
        tasks.add(task2);
        task1.getAccounts().add(account);
        task2.getAccounts().add(account);

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        when(taskRepository.findAllByAccountId(accountId)).thenReturn(tasks);

        // Act
        accountService.delete(accountId);

        // Assert
        verify(taskRepository).findAllByAccountId(accountId);
        verify(taskRepository).saveAll(tasks);
        verify(accountRepository).delete(account);
        assertEquals(0, task1.getAccounts().size());
        assertEquals(0, task2.getAccounts().size());
    }

    @Test
    void givenInvalidWhenDeleteAccountIsFail() {
        Long accountId = 1L;

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        // Act and Assert
        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            accountService.delete(accountId);
        });

        assertEquals(ErrorCodes.ACCOUNT_NOT_FOUND, exception.getErrorCode());
    }
}
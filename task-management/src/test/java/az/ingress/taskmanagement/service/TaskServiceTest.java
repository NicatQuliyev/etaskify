package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.AccountListDto;
import az.ingress.taskmanagement.dto.request.TaskRequest;
import az.ingress.taskmanagement.dto.response.AccountResponse;
import az.ingress.taskmanagement.dto.response.AddressResponse;
import az.ingress.taskmanagement.dto.response.TaskResponse;
import az.ingress.taskmanagement.entity.*;
import az.ingress.taskmanagement.exception.AccountOrOrganizationNotFoundException;
import az.ingress.taskmanagement.exception.ErrorCodes;
import az.ingress.taskmanagement.exception.TaskNotFoundException;
import az.ingress.taskmanagement.repository.AccountRepository;
import az.ingress.taskmanagement.repository.TaskRepository;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.core.KafkaTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

    @InjectMocks
    private TaskService taskService;

    @Mock
    private TaskRepository taskRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private ModelMapper modelMapper;

    @Mock
    private KafkaTemplate<String, String> kafkaTemplate;

    @Test
    void givenAllTasksWhenGetSuccess() {
        //Arrange
        Address firstAddress = new Address();
        Address secondAddress = new Address();
        Organization organization = new Organization();
        List firstPhoneNumbers = List.of("+994502858581", "+994997288873");
        List secondPhoneNumbers = List.of("+994502407737", "+994997288873");

        Account account1 = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddress)
                .organization(organization)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        Account account2 = Account.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddress)
                .organization(organization)
                .phoneNumbers(secondPhoneNumbers)
                .build();

        List<Task> taskList = new ArrayList<>();

        List<Account> accounts = List.of(account1, account2);

        Task mockTask = Task.builder()
                .id(1L)
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 7, 16))
                .taskStatus(TaskStatus.TODO)
                .accounts(accounts)
                .build();

        taskList.add(mockTask);

        when(taskRepository.findAll()).thenReturn(taskList);


        List<TaskResponse> mockTaskResponseList = new ArrayList<>();

        AddressResponse firstAddressResponse = new AddressResponse();
        AddressResponse secondAddressResponse = new AddressResponse();

        AccountResponse accountResponse1 = AccountResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddressResponse)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        AccountResponse accountResponse2 = AccountResponse.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddressResponse)
                .phoneNumbers(secondPhoneNumbers)
                .build();
        List<AccountResponse> accountResponses = List.of(accountResponse1, accountResponse2);

        TaskResponse mockTaskResponse = TaskResponse.builder()
                .id(1L)
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 7, 16))
                .taskStatus(TaskStatus.TODO)
                .accounts(accountResponses)
                .build();

        mockTaskResponseList.add(mockTaskResponse);


        when(modelMapper.map(taskList.get(0), TaskResponse.class)).thenReturn(mockTaskResponseList.get(0));


        //Act
        List<TaskResponse> taskResponses = taskService.findAll();

        //Assert
        assertEquals(mockTaskResponseList, taskResponses);
    }

    @Test
    void givenValidIdWhenGetTasksThenSuccess() {
        //Arrange
        long id = 1L;
        Address firstAddress = new Address();
        Address secondAddress = new Address();
        Organization organization = new Organization();
        List firstPhoneNumbers = List.of("+994502858581", "+994997288873");
        List secondPhoneNumbers = List.of("+994502407737", "+994997288873");

        Account account1 = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddress)
                .organization(organization)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        Account account2 = Account.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddress)
                .organization(organization)
                .phoneNumbers(secondPhoneNumbers)
                .build();

        List<Account> accounts = List.of(account1, account2);

        Task mockTask = Task.builder()
                .id(1L)
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 7, 16))
                .taskStatus(TaskStatus.TODO)
                .accounts(accounts)
                .build();

        when(taskRepository.findById(id)).thenReturn(Optional.of(mockTask));

        AddressResponse firstAddressResponse = new AddressResponse();
        AddressResponse secondAddressResponse = new AddressResponse();

        AccountResponse accountResponse1 = AccountResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddressResponse)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        AccountResponse accountResponse2 = AccountResponse.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddressResponse)
                .phoneNumbers(secondPhoneNumbers)
                .build();
        List<AccountResponse> accountResponses = List.of(accountResponse1, accountResponse2);

        TaskResponse mockTaskResponse = TaskResponse.builder()
                .id(1L)
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 7, 16))
                .taskStatus(TaskStatus.TODO)
                .accounts(accountResponses)
                .build();

        when(modelMapper.map(mockTask, TaskResponse.class)).thenReturn(mockTaskResponse);


        //Act
        TaskResponse taskResponse = taskService.findById(1L);

        //Assert
        assertEquals(taskResponse.getId(), mockTaskResponse.getId());
    }

    @Test
    public void givenInValidIdWhenGetTasksThenException() {
        // Arrange
        Long taskId = 1L;
        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());

        // Act and Assert
        TaskNotFoundException exception = assertThrows(TaskNotFoundException.class, () -> {
            taskService.findById(taskId);
        });

        assertEquals(ErrorCodes.TASK_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void saveTasksWhenSuccess() {
        //Arrange
        Address firstAddress = new Address();
        Address secondAddress = new Address();
        Organization organization = new Organization();
        List firstPhoneNumbers = List.of("+994502858581", "+994997288873");
        List secondPhoneNumbers = List.of("+994502407737", "+994997288873");

        Account account1 = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddress)
                .organization(organization)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        Account account2 = Account.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddress)
                .organization(organization)
                .phoneNumbers(secondPhoneNumbers)
                .build();

        List<Account> accounts = List.of(account1, account2);

        Task mockTask = Task.builder()
                .id(1L)
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 07, 16))
                .taskStatus(TaskStatus.TODO)
                .accounts(accounts)
                .build();

        when(taskRepository.save(any())).thenReturn(mockTask);

        TaskRequest taskRequestDto = TaskRequest.builder()
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 07, 16))
                .taskStatus(TaskStatus.TODO)
                .build();

        when(modelMapper.map(taskRequestDto, Task.class)).thenReturn(mockTask);

        AddressResponse firstAddressResponse = new AddressResponse();
        AddressResponse secondAddressResponse = new AddressResponse();

        AccountResponse accountResponse1 = AccountResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddressResponse)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        AccountResponse accountResponse2 = AccountResponse.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddressResponse)
                .phoneNumbers(secondPhoneNumbers)
                .build();
        List<AccountResponse> accountResponses = List.of(accountResponse1, accountResponse2);

        TaskResponse mockTaskResponse = TaskResponse.builder()
                .id(1L)
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 7, 16))
                .taskStatus(TaskStatus.TODO)
                .accounts(accountResponses)
                .build();

        when(modelMapper.map(mockTask, TaskResponse.class)).thenReturn(mockTaskResponse);


        //Act
        TaskResponse taskResponse = taskService.save(taskRequestDto);


        //Assert
        assertEquals(mockTaskResponse, taskResponse);
    }

    @Test
    void updateTasksWhenSuccess() {
        //Arrange
        Address firstAddress = new Address();
        Address secondAddress = new Address();
        Organization organization = new Organization();
        List firstPhoneNumbers = List.of("+994502858581", "+994997288873");
        List secondPhoneNumbers = List.of("+994502407737", "+994997288873");

        Account account1 = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddress)
                .organization(organization)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        Account account2 = Account.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddress)
                .organization(organization)
                .phoneNumbers(secondPhoneNumbers)
                .build();

        List<Account> accounts = List.of(account1, account2);

        Task mockTask = Task.builder()
                .id(1L)
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 7, 16))
                .taskStatus(TaskStatus.TODO)
                .accounts(accounts)
                .build();

        when(taskRepository.findById(1L)).thenReturn(Optional.of(mockTask));

        TaskRequest taskRequestDto = TaskRequest.builder()
                .title("LMS")
                .description("Learning Management System")
                .deadline(LocalDate.of(2023, 8, 20))
                .taskStatus(TaskStatus.TODO)
                .build();

        when(modelMapper.map(any(TaskRequest.class), eq(Task.class)))
                .thenReturn(mockTask);

        Task updatedTask = Task.builder()
                .id(1L)
                .title("LMS")
                .description("Learning Management System")
                .deadline(LocalDate.of(2023, 8, 20))
                .taskStatus(TaskStatus.TODO)
                .accounts(accounts)
                .build();

        when(taskRepository.save(any(Task.class))).thenReturn(updatedTask);

        AddressResponse firstAddressResponse = new AddressResponse();
        AddressResponse secondAddressResponse = new AddressResponse();

        AccountResponse accountResponse1 = AccountResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddressResponse)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        AccountResponse accountResponse2 = AccountResponse.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddressResponse)
                .phoneNumbers(secondPhoneNumbers)
                .build();
        List<AccountResponse> accountResponses = List.of(accountResponse1, accountResponse2);

        TaskResponse mockTaskResponse = TaskResponse.builder()
                .id(1L)
                .title("LMS")
                .description("Learning Management System")
                .deadline(LocalDate.of(2023, 8, 20))
                .taskStatus(TaskStatus.TODO)
                .accounts(accountResponses)
                .build();

        when(modelMapper.map(any(Task.class), eq(TaskResponse.class)))
                .thenReturn(mockTaskResponse);

        //Act
        taskService.update(1L, taskRequestDto);

        //Assert
        assertThat(mockTaskResponse.getId()).isEqualTo(1L);
        assertThat(mockTaskResponse.getTitle()).isEqualTo("LMS");
        assertThat(mockTaskResponse.getDescription()).isEqualTo("Learning Management System");
        assertThat(mockTaskResponse.getDeadline()).isEqualTo(LocalDate.of(2023,8,20));
        assertThat(mockTaskResponse.getTaskStatus()).isEqualTo(TaskStatus.TODO);
    }

    @Test
    void givenInvalidWhenUpdateTasksThenNotFound() {
        //Arrange
        Long id = 1L;
        when(taskRepository.findById(id)).thenReturn(Optional.empty());

        TaskRequest taskRequest = TaskRequest.builder()
                .title("LMS")
                .description("Learning Management System")
                .deadline(LocalDate.of(2023, 8, 20))
                .taskStatus(TaskStatus.TODO)
                .build();

        //Act&Assert
        TaskNotFoundException exception = assertThrows(TaskNotFoundException.class, () -> {
            taskService.update(id, taskRequest);
        });

        assertEquals(ErrorCodes.TASK_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void deleteAddressWhenSuccess() {
        // Arrange
        Address firstAddress = new Address();
        Address secondAddress = new Address();
        Organization organization = new Organization();
        List firstPhoneNumbers = List.of("+994502858581", "+994997288873");
        List secondPhoneNumbers = List.of("+994502407737", "+994997288873");

        Account account1 = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(firstAddress)
                .organization(organization)
                .phoneNumbers(firstPhoneNumbers)
                .build();

        Account account2 = Account.builder()
                .id(2L)
                .name("Fuad")
                .surname("Quliyev")
                .email("quliyev.fuad@gmail.com")
                .password("qwerty")
                .address(secondAddress)
                .organization(organization)
                .phoneNumbers(secondPhoneNumbers)
                .build();

        List<Account> accounts = List.of(account1, account2);

        Task mockTask = Task.builder()
                .id(1L)
                .title("eTaskify")
                .description("write 2 microservices with email notification that manage tasks of accounts")
                .deadline(LocalDate.of(2023, 7, 16))
                .taskStatus(TaskStatus.TODO)
                .accounts(accounts)
                .build();

        when(taskRepository.findById(mockTask.getId())).thenReturn(Optional.of(mockTask));


        // Act
        taskService.delete(mockTask.getId());

        // Assert
        verify(taskRepository, times(1)).delete(mockTask);
    }

    @Test
    void givenInvalidWhenDeleteAddressIsFail() {
        // Arrange
        Long id = 1L;
        when(taskRepository.findById(id)).thenReturn(Optional.empty());

        // Act&Assert
        TaskNotFoundException exception = assertThrows(TaskNotFoundException.class, () -> {
            taskService.delete(id);
        });

        assertEquals(ErrorCodes.TASK_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    public void testAssignAccountsToTaskWhenTaskFound() {
        // Arrange
        Long organizationId = 1L;
        Long taskId = 1L;
        AccountListDto accountListDto = new AccountListDto();
        List<Long> accountIds = new ArrayList<>();
        accountIds.add(1L);
        accountIds.add(2L);
        accountListDto.setAccountIds(accountIds);

        Task task = new Task();
        Account account1 = new Account();
        Account account2 = new Account();

        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));
        when(accountRepository.findByOrganizationIdAndId(organizationId, 1L)).thenReturn(Optional.of(account1));
        when(accountRepository.findByOrganizationIdAndId(organizationId, 2L)).thenReturn(Optional.of(account2));

        // Act
        taskService.assignAccountsToTask(organizationId, taskId, accountListDto);

        // Assert
        verify(taskRepository).findById(taskId);
        verify(accountRepository).findByOrganizationIdAndId(organizationId, 1L);
        verify(accountRepository).findByOrganizationIdAndId(organizationId, 2L);
        assertEquals(2, task.getAccounts().size());
        assertEquals(account1, task.getAccounts().get(0));
        assertEquals(account2, task.getAccounts().get(1));
        verify(kafkaTemplate, times(2)).send(any(ProducerRecord.class));
    }

    @Test
    public void testAssignAccountsToTaskWhenTaskNotFound() {
        // Arrange
        Long organizationId = 1L;
        Long taskId = 1L;
        AccountListDto accountListDto = new AccountListDto();

        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());

        // Act and Assert
        TaskNotFoundException exception = assertThrows(TaskNotFoundException.class, () -> {
            taskService.assignAccountsToTask(organizationId, taskId, accountListDto);
        });

        assertEquals(ErrorCodes.TASK_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    public void testAssignAccountsToTask_AccountOrOrganizationNotFound() {
        // Arrange
        Long organizationId = 1L;
        Long taskId = 1L;
        AccountListDto accountListDto = new AccountListDto();
        List<Long> accountIds = new ArrayList<>();
        accountIds.add(1L);
        accountListDto.setAccountIds(accountIds);

        Task task = new Task();

        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));
        when(accountRepository.findByOrganizationIdAndId(organizationId, 1L)).thenReturn(Optional.empty());

        // Act and Assert
        AccountOrOrganizationNotFoundException exception = assertThrows(AccountOrOrganizationNotFoundException.class, () -> {
            taskService.assignAccountsToTask(organizationId, taskId, accountListDto);
        });

        assertEquals(ErrorCodes.ACCOUNT_OR_ORGANIZATION_NOT_FOUND, exception.getErrorCode());
    }

}
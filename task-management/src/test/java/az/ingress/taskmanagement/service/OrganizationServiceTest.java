package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.OrganizationRequest;
import az.ingress.taskmanagement.dto.response.OrganizationResponse;
import az.ingress.taskmanagement.entity.Organization;
import az.ingress.taskmanagement.exception.ErrorCodes;
import az.ingress.taskmanagement.exception.OrganizationNotFoundException;
import az.ingress.taskmanagement.repository.OrganizationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrganizationServiceTest {

    @InjectMocks
    private OrganizationService organizationService;

    @Mock
    private OrganizationRepository organizationRepository;
    @Mock
    private ModelMapper modelMapper;

    @Test
    void givenAllOrganizationWhenGetSuccess() {
        //Arrange

        List<Organization> organizationList = new ArrayList<>();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        organizationList.add(mockOrganization);

        when(organizationRepository.findAll()).thenReturn(organizationList);


        List<OrganizationResponse> mockOrganizationResponseList = new ArrayList<>();

        OrganizationResponse mockOrganizationResponse = OrganizationResponse.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        mockOrganizationResponseList.add(mockOrganizationResponse);


        when(modelMapper.map(organizationList.get(0), OrganizationResponse.class)).thenReturn(mockOrganizationResponseList.get(0));


        //Act
        List<OrganizationResponse> organizationResponseList = organizationService.findAll();

        //Assert
        assertEquals(mockOrganizationResponseList, organizationResponseList);
    }

    @Test
    void givenValidIdWhenGetOrganizationThenSuccess() {
        //Arrange
        long id = 1L;


        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        when(organizationRepository.findById(id)).thenReturn(Optional.of(mockOrganization));

        OrganizationResponse mockOrganizationResponse =  OrganizationResponse.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        when(modelMapper.map(mockOrganization, OrganizationResponse.class)).thenReturn(mockOrganizationResponse);


        //Act
        OrganizationResponse organizationResponse = organizationService.findById(1L);

        //Assert
        assertEquals(organizationResponse.getId(), mockOrganizationResponse.getId());
    }

    @Test
    public void givenInValidIdWhenGetOrganizationThenException() {
        // Arrange
        Long organizationId = 1L;
        when(organizationRepository.findById(organizationId)).thenReturn(Optional.empty());

        // Act and Assert
        OrganizationNotFoundException exception = assertThrows(OrganizationNotFoundException.class, () -> {
            organizationService.findById(organizationId);
        });

        assertEquals(ErrorCodes.ORGANIZATION_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void saveOrganizationsWhenSuccess() {
        //Arrange
        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        when(organizationRepository.save(any())).thenReturn(mockOrganization);

        OrganizationRequest organizationRequestDto = OrganizationRequest.builder()
                .organizationName("Yelo Bank")
                .build();

        when(modelMapper.map(organizationRequestDto, Organization.class)).thenReturn(mockOrganization);

        OrganizationResponse mockOrganizationResponse = OrganizationResponse.builder()
                .id(1L)
                .organizationName("20 yanvar")
                .build();

        when(modelMapper.map(mockOrganization, OrganizationResponse.class)).thenReturn(mockOrganizationResponse);


        //Act
        OrganizationResponse organizationResponse = organizationService.save(organizationRequestDto);


        //Assert
        assertEquals(mockOrganizationResponse, organizationResponse);
    }

    @Test
    void updateOrganizationWhenSuccess() {
        //Arrange
        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        when(organizationRepository.findById(1L)).thenReturn(Optional.of(mockOrganization));

        OrganizationRequest organizationRequest = OrganizationRequest.builder()
                .organizationName("ABB")
                .build();

        when(modelMapper.map(any(OrganizationRequest.class), eq(Organization.class)))
                .thenReturn(mockOrganization);

        Organization updatedOrganization = Organization.builder()
                .id(1L)
                .organizationName("ABB")
                .build();

        when(organizationRepository.save(any(Organization.class))).thenReturn(updatedOrganization);

        OrganizationResponse mockOrganizationResponse = OrganizationResponse.builder()
                .id(1L)
                .organizationName("ABB")
                .build();

        when(modelMapper.map(any(Organization.class), eq(OrganizationResponse.class)))
                .thenReturn(mockOrganizationResponse);

        //Act
        organizationService.update(1L, organizationRequest);

        //Assert
        assertThat(mockOrganizationResponse.getId()).isEqualTo(1L);
        assertThat(mockOrganizationResponse.getOrganizationName()).isEqualTo("ABB");
    }

    @Test
    void givenInvalidWhenUpdateOrganizationThenNotFound() {
        //Arrange
        Long id = 1L;
        when(organizationRepository.findById(id)).thenReturn(Optional.empty());

        OrganizationRequest organizationRequest = OrganizationRequest.builder()
                .organizationName("ABB")
                .build();

        //Act&Assert
        OrganizationNotFoundException exception = assertThrows(OrganizationNotFoundException.class, () -> {
            organizationService.update(id, organizationRequest);
        });

        assertEquals(ErrorCodes.ORGANIZATION_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void deleteOrganizationWhenSuccess() {
        // Arrange

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        when(organizationRepository.findById(mockOrganization.getId())).thenReturn(Optional.of(mockOrganization));


        // Act
        organizationService.delete(mockOrganization.getId());

        // Assert
        verify(organizationRepository, times(1)).delete(mockOrganization);
    }

    @Test
    void givenInvalidWhenDeleteOrganizationIsFail() {
        // Arrange
        Long id = 1L;
        when(organizationRepository.findById(id)).thenReturn(Optional.empty());

        // Act&Assert
        OrganizationNotFoundException exception = assertThrows(OrganizationNotFoundException.class, () -> {
            organizationService.delete(id);
        });

        assertEquals(ErrorCodes.ORGANIZATION_NOT_FOUND, exception.getErrorCode());
    }
}
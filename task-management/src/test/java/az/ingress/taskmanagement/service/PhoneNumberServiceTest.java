package az.ingress.taskmanagement.service;

import az.ingress.taskmanagement.dto.request.AddressRequest;
import az.ingress.taskmanagement.dto.request.PhoneNumberRequest;
import az.ingress.taskmanagement.dto.response.AddressResponse;
import az.ingress.taskmanagement.dto.response.PhoneNumberResponse;
import az.ingress.taskmanagement.entity.Account;
import az.ingress.taskmanagement.entity.Address;
import az.ingress.taskmanagement.entity.Organization;
import az.ingress.taskmanagement.entity.PhoneNumber;
import az.ingress.taskmanagement.exception.AccountNotFoundException;
import az.ingress.taskmanagement.exception.AddressNotFoundException;
import az.ingress.taskmanagement.exception.ErrorCodes;
import az.ingress.taskmanagement.exception.PhoneNumberNotFoundException;
import az.ingress.taskmanagement.repository.AccountRepository;
import az.ingress.taskmanagement.repository.AddressRepository;
import az.ingress.taskmanagement.repository.PhoneNumberRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PhoneNumberServiceTest {

    @InjectMocks
    private PhoneNumberService phoneNumberService;

    @Mock
    private PhoneNumberRepository phoneNumberRepository;

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private ModelMapper modelMapper;

    @Test
    void givenAllPhoneNumberWhenGetSuccess() {
        //Arrange

        List<PhoneNumber> numberList = new ArrayList<>();


        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        List phoneNumbers = List.of("+994502858581", "+994997288873");

        Account mockAccount = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();

        PhoneNumber mockPhoneNumber = PhoneNumber.builder()
                .id(1L)
                .phoneNumber("+994502858581")
                .account(mockAccount)
                .build();

        numberList.add(mockPhoneNumber);

        when(phoneNumberRepository.findAll()).thenReturn(numberList);


        List<PhoneNumberResponse> mockPhoneNumberResponseList = new ArrayList<>();

        PhoneNumberResponse mockPhoneNumberResponse = PhoneNumberResponse.builder()
                .id(1L)
                .phoneNumber("+994502858581")
                .build();

        mockPhoneNumberResponseList.add(mockPhoneNumberResponse);


        when(modelMapper.map(numberList.get(0), PhoneNumberResponse.class)).thenReturn(mockPhoneNumberResponseList.get(0));


        //Act
        List<PhoneNumberResponse> phoneNumberResponseList = phoneNumberService.findAll();

        //Assert
        assertEquals(mockPhoneNumberResponseList, phoneNumberResponseList);
    }

    @Test
    void givenValidIdWhenGetPhoneNumberThenSuccess() {
        //Arrange
        long id = 1L;


        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        List phoneNumbers = List.of("+994502858581", "+994997288873");

        Account mockAccount = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();

        PhoneNumber mockPhoneNumber = PhoneNumber.builder()
                .id(1L)
                .phoneNumber("+994502858581")
                .account(mockAccount)
                .build();

        when(phoneNumberRepository.findById(id)).thenReturn(Optional.of(mockPhoneNumber));

        PhoneNumberResponse mockPhoneNumberResponse = PhoneNumberResponse.builder()
                .id(1L)
                .phoneNumber("+994502858581")
                .build();

        when(modelMapper.map(mockPhoneNumber, PhoneNumberResponse.class)).thenReturn(mockPhoneNumberResponse);


        //Act
        PhoneNumberResponse phoneNumberResponse = phoneNumberService.findById(1L);

        //Assert
        assertEquals(phoneNumberResponse.getId(), mockPhoneNumberResponse.getId());
    }

    @Test
    public void givenInValidIdWhenGetAddressThenException() {
        // Arrange
        Long phoneNumberId = 1L;
        when(phoneNumberRepository.findById(phoneNumberId)).thenReturn(Optional.empty());

        // Act and Assert
        PhoneNumberNotFoundException exception = assertThrows(PhoneNumberNotFoundException.class, () -> {
            phoneNumberService.findById(phoneNumberId);
        });

        assertEquals(ErrorCodes.PHONENUMBER_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void savePhoneNumberWhenSuccess() {
        // Arrange
        Long accountId = 1L;

        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        List phoneNumbers = List.of("+994502858581", "+994997288873");

        Account mockAccount = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();

        PhoneNumber mockPhoneNumber = PhoneNumber.builder()
                .id(1L)
                .phoneNumber("+994502858581")
                .account(mockAccount)
                .build();

        PhoneNumberRequest request = PhoneNumberRequest.builder()
                .phoneNumber("+994502858581")
                .build();

        PhoneNumberResponse mockPhoneNumberResponse = PhoneNumberResponse.builder()
                .id(1L)
                .phoneNumber("+994502858581")
                .build();

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(mockAccount));
        when(modelMapper.map(request, PhoneNumber.class)).thenReturn(mockPhoneNumber);
        when(phoneNumberRepository.save(mockPhoneNumber)).thenReturn(mockPhoneNumber);
        when(modelMapper.map(mockPhoneNumber, PhoneNumberResponse.class)).thenReturn(mockPhoneNumberResponse);

        // Act
        PhoneNumberResponse actualResponse = phoneNumberService.save(request, accountId);

        // Assert
        assertEquals(mockPhoneNumberResponse, actualResponse);
    }

    @Test
    public void testSavePhoneNumberAccountNotFound() {
        // Arrange
        Long accountId = 1L;
        PhoneNumberRequest request = new PhoneNumberRequest();

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        // Act and Assert
        AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, () -> {
            phoneNumberService.save(request, accountId);
        });

        assertEquals(ErrorCodes.ACCOUNT_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void updatePhoneNumberWhenSuccess() {
        //Arrange
        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        List phoneNumbers = List.of("+994502858581", "+994997288873");

        Account mockAccount = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();

        PhoneNumber mockPhoneNumber = PhoneNumber.builder()
                .id(1L)
                .phoneNumber("+994502858581")
                .account(mockAccount)
                .build();

        when(phoneNumberRepository.findById(1L)).thenReturn(Optional.of(mockPhoneNumber));

        PhoneNumberRequest phoneNumberRequestDto = PhoneNumberRequest.builder()
                .phoneNumber("+994997288873")
                .build();

        when(modelMapper.map(any(PhoneNumberRequest.class), eq(PhoneNumber.class)))
                .thenReturn(mockPhoneNumber);

        PhoneNumber updatedPhoneNumber = PhoneNumber.builder()
                .id(1L)
                .phoneNumber("+994997288873")
                .account(mockAccount)
                .build();

        when(phoneNumberRepository.save(any(PhoneNumber.class))).thenReturn(updatedPhoneNumber);

        PhoneNumberResponse mockPhoneNumberResponse = PhoneNumberResponse.builder()
                .id(1L)
                .phoneNumber("+994997288873")
                .build();

        when(modelMapper.map(any(PhoneNumber.class), eq(PhoneNumberResponse.class)))
                .thenReturn(mockPhoneNumberResponse);

        //Act
        phoneNumberService.update(phoneNumberRequestDto, 1L);

        //Assert
        assertThat(mockPhoneNumberResponse.getId()).isEqualTo(1L);
        assertThat(mockPhoneNumberResponse.getPhoneNumber()).isEqualTo("+994997288873");
    }

    @Test
    void givenInvalidWhenUpdatePhoneNumberThenNotFound() {
        //Arrange
        Long id = 1L;
        when(phoneNumberRepository.findById(id)).thenReturn(Optional.empty());

        PhoneNumberRequest phoneNumberRequestDto = PhoneNumberRequest.builder()
                .phoneNumber("+994997288873")
                .build();

        //Act&Assert
        PhoneNumberNotFoundException exception = assertThrows(PhoneNumberNotFoundException.class, () -> {
            phoneNumberService.update(phoneNumberRequestDto, id);
        });

        assertEquals(ErrorCodes.PHONENUMBER_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    void deletePhoneNumberWhenSuccess() {
        // Arrange
        Address mockAddress = Address.builder()
                .id(1L)
                .address("20 yanvar")
                .build();

        Organization mockOrganization = Organization.builder()
                .id(1L)
                .organizationName("Yelo Bank")
                .build();

        List phoneNumbers = List.of("+994502858581", "+994997288873");

        Account mockAccount = Account.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .password("qwerty")
                .address(mockAddress)
                .organization(mockOrganization)
                .phoneNumbers(phoneNumbers)
                .build();

        PhoneNumber mockPhoneNumber = PhoneNumber.builder()
                .id(1L)
                .phoneNumber("+994502858581")
                .account(mockAccount)
                .build();

        when(phoneNumberRepository.findById(mockPhoneNumber.getId())).thenReturn(Optional.of(mockPhoneNumber));


        // Act
        phoneNumberService.delete(mockPhoneNumber.getId());

        // Assert
        verify(phoneNumberRepository, times(1)).delete(mockPhoneNumber);
    }

    @Test
    void givenInvalidWhenDeletePhoneNumberIsFail() {
        // Arrange
        Long id = 1L;
        when(phoneNumberRepository.findById(id)).thenReturn(Optional.empty());

        // Act&Assert
        PhoneNumberNotFoundException exception = assertThrows(PhoneNumberNotFoundException.class, () -> {
            phoneNumberService.delete(id);
        });

        assertEquals(ErrorCodes.PHONENUMBER_NOT_FOUND, exception.getErrorCode());
    }
}